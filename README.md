# site-cyberforest
This site is intended to be like a point and click adventure. Not just as Day of the Tentacle or Manic Mansion but more like Putt Putt or Freddi Fish. 

This means: click on an element and it animates. The site isn't there yet, mostly due to lack of time. Pull requests welcome.

Original design made for MCH2021 on request by eisdesigns.

## What have you gotten yourself into...
This repository uses all kinds of modern hipster javascript framework that obsolete quicker than your Apple devices.
For example, just to create this simple website, you need 1600 plugins to function properly: such a tower of 
magnificence, and it somehow works. As long as nobody gets the idea to put NPM inside a pacemaker or other 
Mission Critical Hardware to world is a great place to be.

Here are some commands you can use to get this show on the road.
Ir requires NPM, which you can get from here: https://www.npmjs.com/get-npm

### Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


## Deployment
Deployment manuals for this horrid creation can be found here: https://cli.vuejs.org/guide/deployment.html



