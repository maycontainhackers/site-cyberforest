FROM --platform=$BUILDPLATFORM node:16 AS builder

WORKDIR /code

COPY . /code

RUN npm ci && npm run build

FROM nginx:latest

COPY --from=builder /code/dist/ /usr/share/nginx/html
